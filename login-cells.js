class LoginCells extends Polymer.Element {

  static get is() {
    return 'login-cells';
  }

  static get properties() {
    return {
      userName: {
        type: String,
        notify: true},
      /**
       * userPassword field
       */
      userPassword: {type: String, notify: true},
      /**
       * User IDds
       */
      userId: String,
      _loading: {
        type: Boolean,
        value: false
      },
      clearIcon: {
        type: String,
        value: 'coronita:close'
      },
      /**
       * Icon ID for the show password icon.
       */
      showPwdIcon: {
        type: String,
        value: 'coronita:visualize'
      },
      /**
       * Icon ID for the hide password icon.
       */
      hidePwdIcon: {
        type: String,
        value: 'coronita:hide'
      },
      loggedIn: {
        type: Boolean,
        value: false
      },
      /**
       * Text shown in the loading spinner while `loggedIn` property is `false`.
       */
      loadingText: {
        type: String,
        value: ''
      },
      loggingInText: {
        type: String,
        value: 'Logging in...'
      },
      /**
       * Text shown in the loading spinner when `loggedIn` property is `true`.
       */
      loggedInText: {
        type: String,
        value: 'Logged in'
      },
      _loadingText: {
        type: String,
        computed: '_computeLoadingText(loggedIn)'
      }
    };
  }

  _onFormSubmit(e) {

    this._loading = true;

    this.dispatchEvent(new CustomEvent('request-access', {
      composed: true,
      bubbles: true,
      detail: e.detail
    }));
  }
  _computeLoadingText(loggedIn) {
    return loggedIn ? this.loggedInText : this.loggingInText;
  }
}

customElements.define(LoginCells.is, LoginCells);